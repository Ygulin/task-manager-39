package ru.tsc.gulin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.gulin.tm.dto.request.AbstractRequest;
import ru.tsc.gulin.tm.dto.response.AbstractResponse;

@FunctionalInterface
public interface Operation<RQ extends AbstractRequest, RS extends AbstractResponse> {

    RS execute(@NotNull RQ request);

}

package ru.tsc.gulin.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class DataXmlJaxbSaveRequest extends AbstractUserRequest {

    public DataXmlJaxbSaveRequest(@Nullable final String token) {
        super(token);
    }

}

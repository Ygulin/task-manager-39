package ru.tsc.gulin.tm.exception.entity;

public class ModelNotFoundException extends AbstractEntityNotFoundException {

    public ModelNotFoundException() {
        super("Error! Model not found...");
    }

}

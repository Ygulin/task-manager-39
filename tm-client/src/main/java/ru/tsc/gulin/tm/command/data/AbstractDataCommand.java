package ru.tsc.gulin.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.api.endpoint.IDomainEndpoint;
import ru.tsc.gulin.tm.command.AbstractCommand;

public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    public IDomainEndpoint getDomainEndpoint() {
        return serviceLocator.getDomainEndpoint();
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

}

package ru.tsc.gulin.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import ru.tsc.gulin.tm.api.repository.IRepository;
import ru.tsc.gulin.tm.api.service.IConnectionService;
import ru.tsc.gulin.tm.api.service.IService;
import ru.tsc.gulin.tm.model.AbstractModel;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull SqlSession getSqlSession() {
        return connectionService.getSqlSession();
    }

}

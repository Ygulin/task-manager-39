package ru.tsc.gulin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.model.Session;

import java.util.List;

public interface ISessionService {

    @NotNull
    Session add(@NotNull final Session model);

    @NotNull
    Session add(@Nullable final String userId, @NotNull final Session model);

    void clear(@Nullable final String userId);

    boolean existsById(@Nullable final String userId, @Nullable final String id);

    int getSize(@Nullable final String userId);

    @Nullable
    List<Session> findAll(@Nullable final String userId);

    @Nullable
    Session findOneById(@Nullable final String userId, @Nullable final String id);

    @Nullable
    Session findOneByIndex(@Nullable final String userId, @Nullable final Integer index);

    void remove(@Nullable final String userId, @Nullable final Session model);

    void removeById(@Nullable final String userId, @Nullable final String id);

    void removeByIndex(@Nullable final String userId, @Nullable final Integer index);

}

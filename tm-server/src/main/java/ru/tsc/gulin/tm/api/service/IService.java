package ru.tsc.gulin.tm.api.service;

import ru.tsc.gulin.tm.api.repository.IRepository;
import ru.tsc.gulin.tm.model.AbstractModel;

public interface IService<M extends AbstractModel> extends IRepository<M> {

}
